import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import {Post} from './post';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css'],
  inputs:['post']
})
export class PostComponent implements OnInit {
  @Output() deleteEvent = new EventEmitter<Post>();
  @Output() upadteEvent = new EventEmitter<Post>();
  
 post:Post;
    isEdit : boolean = false;
  editButtonText = 'Edit';
  originalId = '';
  originalTitle = '';

  constructor() { }
   sendDelete(){
    this.deleteEvent.emit(this.post);
  }

   sendUpdate(){
    this.upadteEvent.emit(this.post);
  }
    toggleEdit(){
    this.originalId = this.post.id;
    this.originalTitle = this.post.title;


    if (this.isEdit){
      this.sendUpdate()
    }

    this.isEdit = !this.isEdit;
    this.isEdit ? this.editButtonText = 'Save' : this.editButtonText = 'Edit';
  }

  cancelEdit() {
    this.post.id = this.originalId;
    this.post.title = this.originalTitle;
    this.isEdit = false;
    this.isEdit ? this.editButtonText = 'Save' : this.editButtonText = 'Edit';
  }
  ngOnInit() {
  }

}