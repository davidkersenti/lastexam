import { Component, OnInit } from '@angular/core';
import {PostsService} from './posts.service';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit {

  posts;

  currentPost;
  isLoading = true;

  

    constructor(private _postsService: PostsService) {

      

   // this.posts = this._postsService.getPosts();
  }
   deletePost(post){
    this.posts.splice(
      this.posts.indexOf(post),1
    )

  }
   updatePost(post){
    this.posts.splice(
     this.posts.indexOf(post),1,post

    )
  }
  addPost(post){
    this.posts.push(post)
  }

  ngOnInit() {
     this._postsService.getPosts()
			    .subscribe(posts => {this.posts = posts;
                               this.isLoading = false});
  }

}